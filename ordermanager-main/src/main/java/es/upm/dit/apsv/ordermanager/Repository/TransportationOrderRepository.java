package es.upm.dit.apsv.ordermanager.Repository;

import org.springframework.data.repository.CrudRepository;
import es.upm.dit.apsv.ordermanager.model.TransportationOrder;

//interfaz q extiende del reposiotorio crud (q tiene 5 ops)
public interface TransportationOrderRepository extends CrudRepository<TransportationOrder,String> {
    //TransportationOrder findByTruckAndSt(String truck, int st);
}
