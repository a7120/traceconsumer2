package es.upm.dit.apsv.traceconsumer2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import es.upm.dit.apsv.traceconsumer2.controller.TransportationOrderController;
import es.upm.dit.apsv.traceconsumer2.model.Trace;
import es.upm.dit.apsv.traceconsumer2.model.TransportationOrder;
import es.upm.dit.apsv.traceconsumer2.repository.TraceRepository;
import es.upm.dit.apsv.traceconsumer2.repository.TransportationOrderRepository;

import java.util.function.Consumer;

@SpringBootApplication
public class TraceConsumer2Application {

	public static final Logger log = LoggerFactory.getLogger(TraceConsumer2Application.class);

	@Autowired
	private TraceRepository tr;

	@Autowired
	private TransportationOrderRepository torderRepository;

	@Autowired
	private Environment env;

	public TraceConsumer2Application(TraceRepository tr, TransportationOrderRepository torderRepository,
			Environment env) {
		this.tr = tr;
		this.torderRepository = torderRepository;
		this.env = env;
	}

	public static void main(String[] args) {
		SpringApplication.run(TraceConsumer2Application.class, args);
	}

	@Bean("consumer")
	public Consumer<Trace> checkTrace() {
		return t -> {
			t.setTraceId(t.getTruck() + t.getLastSeen());
			tr.save(t);
			
			TransportationOrder result = null;

			Optional<TransportationOrder> ot = torderRepository.findById(t.getTruck());
			if (ot.isPresent()) 
			   result = ot.get();


			if (result != null && result.getSt() == 0) {

				result.setLastDate(t.getLastSeen());

				result.setLastLat(t.getLat());

				result.setLastLong(t.getLng());

				if (result.distanceToDestination() < 10)

					result.setSt(1);

					torderRepository.save(result);

				log.info("Order updated: " + result);

			}

		};
	}
}
